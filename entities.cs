using System;
using System.Threading;

namespace Rogue {
    public class Key : Entity {
        protected static int count = 0;
        protected int id;

        public Key() 
        {
            this.display = '¶';
            this.id = ++count;
        }
    }
    //// Ground ////
    public class Ground : Entity {
        override public void Display()
        {
            Console.ForegroundColor = this.color;
            Console.BackgroundColor = this.bgColor;
            Console.Write(this.display);
            Console.ResetColor();
        }

        public Ground()
        {
            this.display = ' ';
            this.ghost = true;
        }
    }
    public class LooseDirt : Ground {
        public LooseDirt() : base()
        {
            this.display = '`';
        }
    }
    public class Lava : Ground {
        private int frame = 0;
        private char[] frames = {'~','=','.','o','O','*','`','\''};

        public Lava() : base()
        {
            this.frame = Rand.Int(0, 7);

            this.display = this.frames[this.frame];
            this.color = ConsoleColor.Red;
            this.bgColor = ConsoleColor.DarkRed;
        }

        override public void Run() 
        {
            new Thread(delegate() {
                while (true)
                {
                    Thread.Sleep(800);
                    if (this.frame < (7 - Rand.Int(0,2)))
                        this.frame++;
                    else
                        this.frame = 0;
    
                    this.display = this.frames[this.frame];
                }
            }).Start();
        }

        override public bool Collide(Entity guest) {
            Console.WriteLine("~");
            guest.Damage(13, 18);
            return ghost;
        }
    }

    ////  Obstacles  ////
    public class vWall : Entity {
        public vWall() 
        {
            this.display = '|';
        }
    }
    public class hWall : Entity {
        public hWall()
        {
            this.display = '-';
        }
    }

    public class Boulder : Entity {
        public Boulder()
        {
            this.display = 'O';
        }
    }

    public class Water : Entity {
        public Water()
        {
            this.ghost = true;
            this.display = '~';
        }
    }

    public class Door : BreakableEntity {
        protected Key key;

        public Key Key {
            get { return key; }
        }

        public Door(int health=1, int armor=100) : base(health, armor)
        {
            this.display = '#';
            this.key = new Key();
        }
    }

    ////  Monsters  ////
    public class Goblin : Monster
    {
        public Goblin(int health=4, int armor=1, string name="Goblin")
        {
            this.display = 'g';
            this.damage = 1;
            this.speed = 1;
            this.piercing = 0;

            this.health = health;
            this.armor = armor;
            this.name = name;
        }
    }

    public class Orc : Monster
    {

        public Orc (int health=10, int armor=2, string name="Orc")
        {
            this.display = 'O';
            this.damage = 3;
            this.speed = 1;
            this.piercing = 1;

            this.health = health;
            this.armor = armor;
            this.name = name;
        }
    }

    public class Dragon : Monster
    {
        public Dragon (int health=20, int armor=4, string name="Dragon")
        {
            this.display = 'D';
            this.damage = 10;
            this.speed = 1;
            this.piercing = 10;
            this.color = ConsoleColor.Red;

            this.health = health;
            this.armor = armor;
            this.name = name;
        }

    }
}

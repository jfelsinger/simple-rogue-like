﻿using System;
using System.Threading;

namespace Rogue
{
    public class Tile
    {
        private Entity entity;
        private Entity ground = new Ground();

        public void Display()
        {
            if (this.entity != null) {
                if (this.ground != null)
                    Console.BackgroundColor = ground.BackgroundColor;
                this.entity.Display();
                Console.ResetColor();
            } else if(this.ground != null) {
                this.ground.Display();
            } else {
                Console.Write(' ');
            }
        }

        public void SetGround(Ground ground)
        {
            this.ground = ground;
        }

        public bool Move(Entity guest) 
        {
            if ((this.entity == null) || (this.entity.Collide(guest))) {
                this.entity = guest;
                if (this.ground !=null)
                    this.ground.Collide(guest);
                return true;
            } else
                return false;
        }
        public void clearEntity()
        {
            this.entity = null;
        }

        public void Run()
        {
            if ((this.ground != null) && (this.entity != null))
                this.ground.Collide(this.entity);
        }
    }

    public class Map
    {
        private Tile[,] tiles;
        private int width;
        private int height;

        public int Width {
            get { return width; }
        }

        public int Height {
            get { return height; }
        }

        public Map(int w, int h)
        {
            this.tiles = new Tile[h,w];
            for (int i=0; i < h; i++)
                for (int j=0; j < w; j++)
                    this.tiles[i,j] = new Tile();

            this.width = w;
            this.height = h;
        }

        public void SetGround(Ground ground, int x, int y)
        {
            this.tiles[y, x].SetGround(ground);
        }

        public bool Move(Entity guest, int x, int y)
        {
            int guestX = guest.X;
            int guestY = guest.Y;

            if (this.tiles[y, x].Move(guest))
            {
                this.tiles[guestY, guestX].clearEntity();
                return true;
            }
            else
            {
                return false;
            }
        }

        public void Run() {
            new Thread(delegate() {
                while (true)
                {
                    Thread.Sleep(3400);
                    for (int i=0; i < this.height; i++)
                        for (int j=0; j < this.width; j++)
                            this.tiles[i,j].Run();
                }
            }).Start();
        }

        public void Display() {
            for (int i=0; i < this.height; i++) {
                Console.ForegroundColor = ConsoleColor.White;
                Console.Write("|");
                Console.ResetColor();

                for (int j=0; j < this.width; j++)
                    this.tiles[i,j].Display();

                Console.ForegroundColor = ConsoleColor.White;
                Console.Write("|\r\n");
                Console.ResetColor();
            }
        }
    }
}

using System;
using System.Threading;

namespace Rogue
{
    interface ICombat
    {
        void Attack(Entity obj);
        void Damage(int dmg, int prc);
    }

    /**
     * The base class upon which all other objects in the
     * game are based off of
     */
    public abstract class Entity
    {
        protected int x;
        protected int y;
        protected char display; // Character that represents the entity on the screen
        protected bool ghost = false;
        protected ConsoleColor color = ConsoleColor.Gray; // Color of the entity on the screen
        protected ConsoleColor bgColor = ConsoleColor.Black;

        public ConsoleColor BackgroundColor {
            get { return bgColor; }
        }
        public int X {
            get { return x; }
        }
        public int Y {
            get { return y; }
        }
        public int[] Coords {
            get { return new int[] {x,y}; }
        }
        public bool Ghost {
            get { return ghost; }
        }

        public ConsoleColor Color {
            get { return color; }
            set { color = value; }
        }

        virtual public void Display()
        {
            Console.ForegroundColor = this.color;
            Console.Write(this.display);
            Console.ResetColor();
        }
    
        virtual public bool Collide(Entity guest)
        {
            return this.ghost;
        }

    
        public void Move(Map map, int x, int y)
        {
            if (map.Move(this, x, y)) {
                this.x = x;
                this.y = y;
            }
        }

        public virtual void Die() {}
        public virtual void Run() {}
        public virtual void Run(Map map) {}
        public virtual void Damage(int dmg, int prc) {}
        public virtual void Attack(Entity target) {}
    }
    
    /**
     * Defines objects that can be broken through or otherwise destroyed
     */
    public abstract class BreakableEntity : Entity
    {
        protected int health;
        protected int armor;

        public int Health {
            get { return health; }
        }

        public int Armor {
            get { return armor; }
        }
    
        public BreakableEntity(int health, int armor)
        {
            this.health = health;
            this.armor = armor;
        }

        override public void Damage(int dmg, int prc)
        {
            if ((this.armor - prc) > 0)
            {
                if (dmg > (this.armor - prc))
                    this.health -= (dmg - (this.armor - prc));
            }
            else
                this.health -= dmg;
        }

        override public bool Collide(Entity guest) {
            guest.Attack(this);
            if (this.health > 0)
                return ghost;
            else
                return true;
        }

        override public void Die() { /* what to do when health reaches 0 */ }
    }
    
    public abstract class Life: Entity, ICombat
    {
        protected string name;
        protected int health = 1;
        protected int armor = 0;
        protected int damage = 0;
        protected int speed = 1;
        protected int piercing = 0;

        public string Name {
            get { return name; }
        }

        public int Health {
            get { return health; }
        }

        override public void Attack(Entity target)
        {
            target.Damage(this.damage, this.piercing);
        }

        override public void Damage(int dmg, int prc)
        {
            if ((this.armor - prc) > 0)
            {
                if (dmg > (this.armor - prc))
                    this.health -= (dmg - (this.armor - prc));
            }
            else
                this.health -= dmg;
        }

        override public void Run() { /* What it does each turn */ }

        override public void Die() { /* What to do when health = 0 */ }

        override public bool Collide(Entity guest) {
            guest.Attack(this);
            if (this.health > 0)
                return ghost;
            else
                return true;
        }
    }
    
    public abstract class Monster : Life, ICombat
    {
        // Basic enemies, they can hurt you
        public Monster()
        {
            this.color = ConsoleColor.DarkGreen;
        }

        override public void Attack(Entity target)
        {
            if (!(target is Monster))
                target.Damage(this.damage, this.piercing);
        }


        override public void Run(Map map)
        {
            new Thread(delegate() {
                while (this.health > 0)
                {
                    Thread.Sleep(800/this.speed);
                    switch (Rand.Int(1,4))
                    {
                        case 1:
                            for (int i = 0; i < this.speed; i++)
                                if (this.y > 0)
                                    Move(map, this.x, this.y - 1);
                                else
                                    break;
                            break;
                        case 2:
                            for (int i = 0; i < this.speed; i++)
                                if (this.y < map.Height - 1)
                                    Move(map, this.x, this.y + 1);
                                else
                                    break;
                            break;
                        case 3:
                            for (int i = 0; i < this.speed; i++)
                                if (this.x > 0)
                                    Move(map, this.x - 1, this.y);
                                else
                                    break;
                            break;
                        case 4:
                            for (int i = 0; i < this.speed; i++)
                                if (this.x < map.Width - 1)
                                    Move(map, this.x + 1, this.y);
                                else
                                    break;
                            break;
                    }
                }
            }).Start();
        }

    }
    
    public abstract class Ally : Life, ICombat
    {
        // Allies don't hurt you, but you can't move through them
        public Ally()
        {
            this.color = ConsoleColor.Cyan;
        }

        override public void Attack(Entity target)
        {
            if (!(target is Ally) || !(target is Player))
                target.Damage(this.damage, this.piercing);
        }
    }
    
    public class Player : Life, ICombat
    {
        const int defaultMaxHealth = 5;
        protected int maxHealth = 5;

        public Player(int health=defaultMaxHealth) {
            this.health = health;
            this.color = ConsoleColor.DarkRed;
            this.display = '@';
            this.speed = 3;
            this.damage = 1;
        }

        public int MaxHealth {
            get { return maxHealth; }
        }

        override public void Run(Map map) {
            new Thread(delegate() {
                while (this.health > 0)
                {
                    // Thread.Sleep(600 / this.speed);
                    ConsoleKeyInfo action = Console.ReadKey();
                    switch (action.Key)
                    {
                        // Movement with arrow keys, holding shift to run at full speed
                        case ConsoleKey.UpArrow:
                            if  (action.Modifiers == ConsoleModifiers.Shift)
                                for (int i = 0; i < this.speed; i++)
                                    if (this.y > 0)
                                        Move(map, this.x, this.y - 1);
                                    else
                                        break;
                            else if (this.y > 0)
                                Move(map, this.x, this.y - 1);
                            break;
                        case ConsoleKey.DownArrow:
                            if  (action.Modifiers == ConsoleModifiers.Shift)
                                for (int i = 0; i < this.speed; i++)
                                    if (this.y < map.Height-1)
                                        Move(map, this.x, this.y + 1);
                                    else
                                        break;
                            else if (this.y < map.Height-1)
                                Move(map, this.x, this.y + 1);
                            break;
                        case ConsoleKey.LeftArrow:
                            if  (action.Modifiers == ConsoleModifiers.Shift)
                                for (int i = 0; i < this.speed; i++)
                                    if (this.x > 0)
                                        Move(map, this.x - 1, this.y);
                                    else
                                        break;
                            else if (this.x > 0)
                                Move(map, this.x - 1, this.y);
                            break;
                        case ConsoleKey.RightArrow:
                            if (action.Modifiers == ConsoleModifiers.Shift)
                                for (int i = 0; i < this.speed; i++)
                                    if (this.x < map.Width - 1)
                                        Move(map, this.x + 1, this.y);
                                    else
                                        break;
                            else if (this.x < map.Width - 1)
                                Move(map, this.x + 1, this.y);
                            break;
                    }
                }
            }).Start();
        }
    }
}

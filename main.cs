using System;
using System.Collections.Generic;
using System.Threading;

namespace Rogue
{
    static class Program
    {
        static void Main()
        {
            Game game = new Game();
            // Console.CancelKeyPress += new ConsoleCancelEventHandler(Console_CancelKeyPress);
            Console.CursorVisible = false;


            game.Run();
            while (game.Running)
            {
                game.Display();
                Thread.Sleep(180);
            }

            Console.ReadLine();
        }
    }

    public class Rand
    {
        private static readonly Random random = new Random();
        private static readonly object syncLock = new object();
        public static int Int(int min, int max)
        {
            lock(syncLock) {
                return random.Next(min, max);
            }
        }
    }

    public class Game 
    {
        private Map map;
        private Player player;
        private bool running = true;
        private List<Entity> entities;

        public bool Running
        {
            get { return running; }
        }

        public Game() 
        {
            this.entities = new List<Entity>();
            // create the map and the player
            this.map = new Map(30, 10);
            this.player = new Player();

            // Instatize a monster or two
            this.Add(new Dragon(200, 12, "Alduin"), 22, 3);

            // Set things in place
            this.player.Move(this.map, 2, 2);

            this.Add(new Lava(), 9, 5);
            this.Add(new Lava(), 10, 3);
            this.Add(new Lava(), 10, 4);
            this.Add(new Lava(), 10, 3);
            this.Add(new Lava(), 11, 5);
            this.Add(new Lava(), 11, 4);
            this.Add(new Lava(), 11, 3);
            this.Add(new Lava(), 12, 5);
            this.Add(new Lava(), 12, 4);
            this.Add(new Lava(), 12, 3);
            this.Add(new Lava(), 13, 5);
            this.Add(new Lava(), 13, 4);
            this.Add(new Lava(), 13, 3);
            this.Add(new Lava(), 14, 5);
            this.Add(new Lava(), 14, 4);
            this.Add(new Lava(), 14, 3);
            this.Add(new Lava(), 15, 3);
            this.Add(new Lava(), 15, 4);
        }

        public void Add(Entity entity, int x, int y) {
            this.entities.Add(entity);
            entity.Move(this.map, x, y);
        }

        public void Add(Ground ground, int x, int y) {
            this.entities.Add(ground);
            this.map.SetGround(ground, x, y);
        }

        public void Run()
        {
            this.player.Run(this.map);
            foreach (Entity entity in this.entities)
            {
                if (entity is Life)
                    entity.Run(this.map);
                else
                    entity.Run();
            }
        }

        public void Display() 
        {
            Console.Clear();
            Console.ForegroundColor = ConsoleColor.White;
            Console.Write("*");
            for (int i=0; i < this.map.Width; i++)
                Console.Write("=");
            Console.Write("*\r\n");
            Console.ResetColor();

            this.map.Display();

            Console.ForegroundColor = ConsoleColor.White;
            Console.Write("*");
            for (int i=0; i < this.map.Width; i++)
                Console.Write("=");
            Console.Write("*\r\n");

            // Display Player's Health
            Console.Write("hp:");
            Console.ForegroundColor = ConsoleColor.Red;
            for (int i = 0; i < this.player.MaxHealth - Math.Max(this.player.Health, 0); i++)
                Console.Write("-");
            for (int i = 0; i < this.player.Health; i++)
                Console.Write("*");
            Console.ResetColor();
            Console.Write("\r\n");

            Console.ResetColor();
        }
    }
}
